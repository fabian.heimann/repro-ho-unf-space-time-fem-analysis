# Reproduction material for "NUMERICAL ANALYSIS OF GEOMETRICALLY HIGHER ORDER UNFITTED SPACE-TIME METHODS FOR PDES ON MOVING DOMAINS" by F. Heimann, C. Lehrenfeld and J. Preuß

This repository contains the scripts necessary to reproduce the data of the numerical experiments of the paper.

The computations are based on the software ngsxfem, where we used the commit 7bc5e4353c0fb919dd6c46df76769f518dc124ad.

## Setup ngsxfem to run the examples.
Our necessary software can be supplied in several forms. Firstly, it can be downloaded from [`here`](http://github.com/ngsxfem/ngsxfem). Morevoer, one could use the Dokerfile presented in this related [`repository`](https://gitlab.gwdg.de/fabian.heimann/repro-ho-unf-space-time-fem).

## Reproduction scripts
The commands to perform the numerical studies of the paper are collected in the bash file `runs.sh`. In order to avoid numerical differences due to different (but similarly suitable) meshes, the scripts exploit pre-generated and loaded meshes. Because of file size reasons, the two largest 3D meshes are not contained and can be downloaded from https://doi.org/10.25625/LSA3U0 .
