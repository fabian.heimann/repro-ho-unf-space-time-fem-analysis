from xfem import *

from solve_DG import *

f = open("out/conv_K.dat", "w")

for K_exp in range(-9,10):
    l2final, l2l2er, mderr, minnzes, maxnzes = SolveDG(2, 2, 3, 3, 3, "kite", gamma=0.05, K=10**K_exp, struct_mesh=2, n_threads=6, solver="umfpack", calc_max_dist=0)
    f.write(str(10**K_exp)+"\t"+str(l2final)+"\t"+str(mderr)+"\n")
