from ngsolve import *
from xfem import *
from math import pi
from ngsolve.solvers import *
from ngsolve.meshes import *
from xfem.lset_spacetime import *
# from netgen import gui

def SolveDG(i_t=2, i_s=2, k_t = 1, k_t_lset = 1, k_s = 3, geom='kite', alpha=1., gamma=0.05, K=1, struct_mesh = 1, n_threads = 6, solver="umfpack", exact_nze_est = False, calc_max_dist = False):
    
    #ngsglobals.msg_level = 2
    SetNumThreads(n_threads)
    
    n_steps = 2**(i_t if struct_mesh == 1 else i_t+2)
    space_refs = i_s
    
    # Polynomial order in time for level set approximation
    lset_order_time = k_t_lset
    # Integration order in time
    time_order = 2 * max( k_t, k_t_lset) +2
    # Time stepping parameters
    tstart = 0
    tend = 0.5
    delta_t = (tend - tstart) / n_steps
    # Map from reference time to physical time
    told = Parameter(tstart)
    t = told + delta_t * tref

    if geom == 'moving_circle':
        from netgen.geom2d import SplineGeometry
        # Outer domain:
        geometry = SplineGeometry()
        geometry.AddRectangle([-0.6, -1], [0.6, 1])
        maxh = 0.5
        
        D = 2
        
        (xmin, xmax) = (-0.6, 0.6)
        (ymin, ymax) = (-1, 1)

        # Level set geometry
        # Radius of disk (the geometry)
        R = 0.5
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((0, rho.Diff(t)))
        max_velocity = 2.
        
        # Level set
        r = sqrt(x**2 + (y - rho)**2)
        levelset = r - R

        # Diffusion coefficient
        # alpha = 1
        # Solution
        u_exact = cos(pi * r / R) #* sin(pi * t)
        # R.h.s.
        coeff_f = (u_exact.Diff(t)
                - alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y))
                + w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y)).Compile()
    
    elif geom == 'moving_int':
        D = 1
        
        (xmin, xmax) = (-1, 1)

        # Level set geometry
        # Radius of disk (the geometry)
        R = 0.5
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((rho.Diff(t)))
        max_velocity = 2.
        
        # Level set
        r = sqrt((x-rho)**2)
        levelset = r - R

        # Diffusion coefficient
        # alpha = 1
        # Solution
        u_exact = cos(pi * r / R) * sin(pi * t)
        # R.h.s.
        coeff_f = (u_exact.Diff(t) - alpha * (u_exact.Diff(x).Diff(x)) + w[0] * u_exact.Diff(x)).Compile()
        
    elif geom == 'moving_int_sp':
        D = 1
        
        (xmin, xmax) = (-1, 1)

        # Level set geometry
        # Radius of disk (the geometry)
        R = 0.505
        # Position shift of the geometry in time
        rho = 0.5*t #(1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((rho.Diff(t)))
        
        # Level set
        r = sqrt((x-rho)**2)
        levelset = r - R

        # Diffusion coefficient
        # alpha = 1
        # Solution
        u_exact = (x-rho+R)**2*(x-rho-R)**2 
        #u_exact = cos(pi * r / R) * sin(pi * t)
        # R.h.s.
        coeff_f = (u_exact.Diff(t) - alpha * (u_exact.Diff(x).Diff(x)) + w[0] * u_exact.Diff(x)).Compile()
    
    elif geom == 'collid_circles':
        from netgen.geom2d import SplineGeometry
        # Outer domain:
        geometry = SplineGeometry()
        geometry.AddRectangle([-0.6, -1.35], [0.6, 1.35])
        maxh = 0.5
        
        tend = 1.5
        
        D = 2
        
        (xmin, xmax) = (-0.6, 0.6)
        (ymin, ymax) = (-1.35, 1.35)

        # alpha = 0.1
        print("Attention: For collid_circles, we now use alpha = ", alpha)

        # Level set geometry
        # Radius of disk (the geometry)
        R = 0.5
        
        # Level set
        r1 = sqrt(x**2 + (y - t + 0.75)**2)
        r2 = sqrt(x**2 + (y + t - 0.75)**2)
        levelset = IfPos(r1-r2, r2, r1) - R
        
        # Convection velocity:
        w_start = IfPos(y, CoefficientFunction((0,1)), CoefficientFunction((0,-1)))
        w_end = IfPos(y, CoefficientFunction((0,-1)), CoefficientFunction((0,1)))
        w = IfPos( t - tend/2, w_end, w_start)
        
        u_exact = IfPos(y, 1, -1)
        coeff_f = 0.
    
    elif geom == 'kite':
        from netgen.geom2d import SplineGeometry
        geometry = SplineGeometry()
        geometry.AddRectangle([-1.05,-1.05],[1.55,1.05])
        maxh = 0.9
        
        D = 2
        
        (xmin, xmax) = (-1.05, 1.55)
        (ymin, ymax) = (-1.05, 1.55)
        
        r0 = 1
    
        rho = (1 - y**2)*t                                   
    
        # alpha = 1
        #convection velocity:
        w = CoefficientFunction((rho.Diff(t),0))
        max_velocity = 1
    
        # level set
        r = sqrt((x- rho)**2+y**2)
        levelset= r - r0

        Q = pi/r0   
        u_exact = cos(Q*r) * sin(pi*t)
        
        coeff_f = (u_exact.Diff(t)
                - alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y))
                + w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y)).Compile()
    
    elif geom == 'moving_sphere':
        from ngsolve.meshes import OrthoBrick, Pnt, CSGeometry
        geometry = CSGeometry()
        geometry.Add (OrthoBrick(Pnt(-0.6,-1,-0.6), Pnt(0.6,1,0.6)))
        maxh = 0.5
        
        D = 3
        
        (xmin, xmax) = (-0.6, 0.6)
        (ymin, ymax) = (-0.85, 0.85)
        (zmin, zmax) = (-0.6, 0.6)
        
        # Level set geometry
        # Radius of sphere (the geometry)
        R = 0.5
        # Position shift of the geometry in time
        rho = (1 / (pi)) * sin(2 * pi * t)
        # Convection velocity:
        w = CoefficientFunction((0, rho.Diff(t), 0))
        max_velocity = 2
        
        # Level set
        r = sqrt(x**2 + (y - rho)**2 + z**2)
        levelset = r - R

        # Diffusion coefficient
        # alpha = 1
        # Solution
        u_exact = cos(pi * r / R) * sin(pi * t)
        # R.h.s.
        coeff_f = (u_exact.Diff(t)
                - alpha * (u_exact.Diff(x).Diff(x) + u_exact.Diff(y).Diff(y) + u_exact.Diff(z).Diff(z))
                + w[0] * u_exact.Diff(x) + w[1] * u_exact.Diff(y) + w[2] * u_exact.Diff(z)).Compile()
        
    
    # ----------------------------------- MAIN ------------------------------------
    if struct_mesh == 1:
        if D == 1:
            mesh = Make1DMesh(n=2**(space_refs+1), mapping= lambda x : (xmax - xmin) *x + xmin)
        elif D == 2:
            mesh = MakeStructured2DMesh(quads=False,nx=2**(space_refs+1),ny=2**(space_refs+1), mapping= lambda x,y : ( (xmax - xmin) *x + xmin, (ymax - ymin) *y + ymin))
        elif D == 3:
            mesh = MakeStructured3DMesh(hexes=False,nx=2**(space_refs+1),ny=2**(space_refs+1), nz=2**(space_refs+1), mapping= lambda x,y,z : ( (xmax - xmin) *x + xmin, (ymax - ymin) *y + ymin, (zmax - zmin) *z + zmin))
        else:
            Exception("Dims 2 and 3 allowed only")
    elif struct_mesh == 0:
        ngmesh = geometry.GenerateMesh(maxh=maxh*0.5**space_refs, quad_dominated=False)
        #ngmesh = geometry.GenerateMesh(maxh=maxh, quad_dominated=False)
        #for j in range(space_refs):
            #ngmesh.Refine()
        mesh = Mesh(ngmesh)
    elif struct_mesh == 2:
        mesh = Mesh("mesh_"+geom+"i_s"+str(space_refs)+".vol.gz")

    print("Initial f norm: ", sqrt( Integrate(fix_tref(coeff_f,0)**2*dx, mesh)))
    print("alpha: ", alpha)

    # Spatial FESpace for solution
    fes1 = H1(mesh, order=k_s, dgjumps=True)
    # Time finite element (nodal!)
    tfe = ScalarTimeFE(k_t)
    # Scalar Number space for mass conservation
    N = NumberSpace(mesh)
    # st_fes_simple = tfe * fes1
    # (Tensor product) space-time finite element space
    st_fes = (tfe * fes1) *N

    # Space time version of Levelset Mesh Adapation object. Also offers integrator
    # helper functions that involve the correct mesh deformation
    lsetadap = LevelSetMeshAdaptation_Spacetime(mesh, order_space=k_s,
                                                order_time=lset_order_time,
                                                threshold=0.5,
                                                discontinuous_qn=True)

    gfu = GridFunction(st_fes)
    gfu_main = gfu.components[0]
    u_last = CreateTimeRestrictedGF(gfu_main, 1)
    fes_l2 = L2(mesh, order=k_s, dgjumps=True)
    u_last_l2 = GridFunction(fes_l2)

    if mesh.dim > 1:
        scene = DrawDC(lsetadap.levelsetp1[TOP], u_last, 0, mesh, "u_last",
                deformation=lsetadap.deformation[TOP])

    (u, mu), (v, lam) = st_fes.TnT()
    h = specialcf.mesh_size
    init_mass = Parameter(0.)
    vol = Parameter(1.)
    
    ba_facets = BitArray(mesh.nfacet)
    ci = CutInfo(mesh, time_order=0)

    dQ = delta_t * dCut(lsetadap.levelsetp1[INTERVAL], NEG, time_order=time_order,
                        deformation=lsetadap.deformation[INTERVAL],
                        definedonelements=ci.GetElementsOfType(HASNEG))
    dOmold = dCut(lsetadap.levelsetp1[BOTTOM], NEG,
                deformation=lsetadap.deformation[BOTTOM],
                definedonelements=ci.GetElementsOfType(HASNEG), tref=0)
    dOmnew = dCut(lsetadap.levelsetp1[TOP], NEG,
                deformation=lsetadap.deformation[TOP],
                definedonelements=ci.GetElementsOfType(HASNEG), tref=1)
    dw = delta_t * dFacetPatch(definedonelements=ba_facets, time_order=time_order,
                            deformation=lsetadap.deformation[INTERVAL])


    def dt(u):
        return 1.0 / delta_t * dtref(u)

    a = RestrictedBilinearForm(st_fes, "a", check_unused=False,
                            element_restriction=ci.GetElementsOfType(HASNEG),
                            facet_restriction=ba_facets)
    a += v * (dt(u) - dt(lsetadap.deform) * grad(u)) * dQ
    a += (alpha * InnerProduct(grad(u), grad(v))) * dQ
    a += (v * InnerProduct(w, grad(u))) * dQ
    a += u * v * dOmold
    a += h**(-2) * (1 + delta_t / h) * gamma * \
        (u - u.Other()) * (v - v.Other()) * dw
    
    a += (u-mu)*lam*dOmnew
    a += vol*K*mu*v*dOmnew
    
    f = LinearForm(st_fes)
    f += coeff_f * v * dQ
    f += u_last_l2 * v * dOmold
    f += K* init_mass* v * dOmnew

    # Set initial values
    u_last_l2.Set(fix_tref(u_exact, 0))
    # Project u_last at the beginning of each time step
    lsetadap.ProjectOnUpdate(u_last_l2)
    
    lsetadap.CalcDeformation(levelset)
    init_mass.Set( Integrate(u_last_l2 * dCut(lsetadap.levelsetp1[BOTTOM], NEG, deformation=lsetadap.deformation[BOTTOM], tref=0, order=2*k_s), mesh))
    
    maxdists = []
    l2l2_helper_sum = 0
    firstrun = True

    while tend - told.Get() > delta_t / 2:
        lsetadap.CalcDeformation(levelset)
        
        if calc_max_dist:
            mesh.SetDeformation(lsetadap.deformation[INTERVAL])
            maxdists.append(lsetadap.CalcMaxDistance(levelset))
            mesh.UnsetDeformation()
        
        navie_timeint_tmp = ngsxfemglobals.do_naive_timeint
        ngsxfemglobals.do_naive_timeint = False
        # Update markers in (space-time) mesh
        ci.Update(lsetadap.levelsetp1[INTERVAL], time_order=0)
        ngsxfemglobals.do_naive_timeint = navie_timeint_tmp
        
        # re-compute the facets for stabilization:
        ba_facets[:] = GetFacetsWithNeighborTypes(mesh,
                                            a=ci.GetElementsOfType(HASNEG),
                                            b=ci.GetElementsOfType(HASNEG)) # ci.GetElementsOfType(IF))
        active_dofs = GetDofsOfElements(st_fes, ci.GetElementsOfType(HASNEG))
        
        #vol.Set( Integrate( CF(1.)*dOmnew, mesh))
        vol.Set( Integrate(CF(1.)*dCut(lsetadap.levelsetp1[TOP], NEG, deformation=lsetadap.deformation[TOP], definedonelements=ci.GetElementsOfType(HASNEG), tref=1, order=2*k_s), mesh) )
        init_mass.Set( init_mass.Get() + Integrate(coeff_f * dQ, mesh) )
        #init_mass.Set( Integrate(u_exact * dCut(lsetadap.levelsetp1[TOP], NEG, deformation=lsetadap.deformation[TOP], definedonelements=ci.GetElementsOfType(HASNEG), tref=1, order=2*k_s), mesh) )
        #init_mass.Set( Integrate( u_last * dCut(lsetadap.levelsetp1[TOP], NEG, deformation=lsetadap.deformation[TOP], definedonelements=ci.GetElementsOfType(HASNEG), tref=1, order=2*k_s), mesh) )
        # print("init mass (end of ts): ", init_mass)
        
        with TaskManager():
            a.Assemble(reallocate=True)
            f.Assemble()
        
        # Solve linear system
        inv = a.mat.Inverse(active_dofs, inverse=solver)
        #gfu.vec.data = inv * f.vec.data
        #inv = a_i.mat.Inverse(active_dofs, solver)
        gfu.vec.data = GMRes(a.mat, f.vec, inv, tol=1e-12, printrates=False,maxsteps=5000)

        #print("mu: ",gfu.components[1](mesh(0.5,0.5)))
        #u_shape_tester = GridFunction(gfu.components[0].space)
        #u_shape_tester.vec[:] = 0
        #u_shape_tester.vec[299] = 1
        #print("mass contrib of first shape fun: ", Integrate(u_shape_tester*dOmnew, mesh))
        #print("a.mat[-1,0]", a.mat[len(u_shape_tester.vec), 299])
        
        #print("f.vec[-1]: ", f.vec[-1])
        
        #print("mu, calculated: ", Integrate(gfu.components[0] * dOmnew, mesh))
        #print("Calc diff: ", Integrate( (gfu.components[0] ) * dOmnew, mesh)- init_mass.Get())
        # exit()

        # Evaluate upper trace of solution for
        #  * for error evaluation
        #  * upwind-coupling to next time slab
        RestrictGFInTime(spacetime_gf=gfu_main, reference_time=1.0, space_gf=u_last)
        u_last_l2.Set(u_last)
        # Draw(gfu.components[1], mesh, "mu")
        
        # Compute error at final time. Unlike with the so-far defined region, we would like to pick a space integration order, 2 k_s, manually here.
        l2error = sqrt(Integrate((u_exact - u_last)**2 * dCut(lsetadap.levelsetp1[TOP], NEG, deformation=lsetadap.deformation[TOP], definedonelements=ci.GetElementsOfType(HASNEG), tref=1, order=2*k_s), mesh))
        l2l2_helper_sum += Integrate( ( (CF( (u_exact.Diff(x), u_exact.Diff(y)) ) - grad(gfu_main))**2  +  (u_exact.Diff(t) - dt(gfu_main) + dt(lsetadap.deform) * grad(gfu_main))**2)* delta_t * dCut(lsetadap.levelsetp1[INTERVAL], NEG, time_order=time_order, deformation=lsetadap.deformation[INTERVAL], definedonelements=ci.GetElementsOfType(HASNEG), order=2*k_s), mesh)
        md_error = Integrate( u_last * dCut(lsetadap.levelsetp1[TOP], NEG, deformation=lsetadap.deformation[TOP], definedonelements=ci.GetElementsOfType(HASNEG), tref=1, order=2*k_s), mesh) - init_mass.Get()
        
        if exact_nze_est:
            cnt = 0
            for ii in range(len(a.mat.AsVector())):
                if (a.mat.AsVector()[ii] != 0.0):
                    cnt += 1
        else:
            cnt = a.mat.nze
        
        if firstrun:
            minnze = cnt
            maxnze = cnt
            firstrun = False
        else:
            if cnt > maxnze:
                maxnze = cnt
            if cnt < minnze:
                minnze = cnt

        # Update time variable (ParameterCL)
        told.Set(told.Get() + delta_t)
        print("\rt = {0:12.9f}, L2 error = {1:12.9e}".format(told.Get(), l2error))
    
    if calc_max_dist:
        return (l2error, sqrt(l2l2_helper_sum), abs(md_error), minnze, maxnze, max(maxdists))
    else:
        return (l2error, sqrt(l2l2_helper_sum), abs(md_error), minnze, maxnze)
